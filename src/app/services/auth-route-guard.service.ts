import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Events } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  signedIn = false;

  private userSubscription: Subscription;

  constructor(
    public router: Router,
    public events: Events,
    private authService: AuthService
  ) {
    // not working
    this.events.subscribe('authState', async (data) => {
      console.log('event: authState');
      console.log(data);
      if (data.loggedIn) {
        // this.signedIn = true;
      } else {
        // this.signedIn = false;
      }
    });
    //
    this.userSubscription = this.authService.userState
    .subscribe(data => {
      console.log('subscription: data:AuthState');
      console.log(data);
      if (data.loggedIn) {
        this.signedIn = true;
      } else {
        this.signedIn = false;
      }
    });
  }

  // Get latest status from service
  canActivate(): Promise<boolean> {
    // return this.signedIn;
    return this.authService.getAuthState();
  }

}

import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { AmplifyService } from 'aws-amplify-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState: any;
  amplifyService: AmplifyService;

  public userState: BehaviorSubject<any> = new BehaviorSubject<any>({ loggedIn: false });

  constructor(
    public events: Events,
    public amplify: AmplifyService
  ) {
    this.authState = { loggedIn: false };
    this.amplifyService = amplify;
    this.amplifyService.authStateChange$
      .subscribe(authState => {
        console.log(authState);
        this.authState.loggedIn = authState.state === 'signedIn';
        this.userState.next(this.authState);
        this.events.publish('authState', this.authState);
      });
  }

  public getAuthState(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.amplifyService.authStateChange$
        .subscribe(authState => {
          console.log(authState);
          if (authState.state === 'signedIn') {
            console.log('Signed In');
            resolve(true);
          }
          console.log('Signed Out');
          resolve(false);
        });
    });
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  // { path: 'list.page', loadChildren: './pages/list/list.page/list.page.module#List.PagePageModule' },
  // { path: 'list', loadChildren: './pages/list/list.module#ListPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
